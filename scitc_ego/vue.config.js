module.exports = {
    devServer :{
        // 配置代理(proxy小写),配置完毕，必须手动重新编译
        proxy:{
            '/api':{
                target:'http://zhou.com:8000/',
                changeOrigin:true,      //是否跨域
                // ws:true,
                // secure:true,//如果接口是https，需要配置？
                // pathRewrite:{
                //     '^/api':'/'
                // }
            },
            '/storage':{
                target:'http://zhou.com:8000/'

            }
        }
    }
}