import Vue from 'vue'
import App from './App.vue'
import router from './router'   //引入路由配置Router
import store from './store'     //引入状态管理Vuex


// 1、引用axios
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

// 2、引入Vant（UI界面）
import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant)
Vue.config.productionTip = false

// 3、引入less(vue2.6.11,less@3.9.0，less-loader@5.0.0)
import Less from 'less'
Vue.use(Less)

// 引入qs
import qs from 'qs'


// 添加拦截器：对axios的请求和响应做出检查
// 1.请求拦截
axios.interceptors.request.use(
  config => {
    //???
    if (sessionStorage.token) {
      // config.headers = {
      //   'api_token' : sessionStorage.token,
      //   'id' : sessionStorage.id
      // }
      // }
      if (config.method == 'get') {
        config.params = {
          api_token: sessionStorage.token,
          id: sessionStorage.id
        }
      }
      // if (config.method == 'post' || config.method == 'delete') {
      //   config.data = {
      //     api_token: sessionStorage.token,
      //     id: sessionStorage.id
      //   }
      // }
      if(config.method === 'post'){   //如果是post，一般需要使用qs进行编码
        // 因为post大都需要认证，此处需要传递token和id
        config.data.api_token = sessionStorage.token
        config.data.id = sessionStorage.id
        // 对发送的数据进行编码
        config.data = qs.stringify(config.data)
      }
    }
    return config;
  },
  err => {
    return Promise.reject(err)
  }
);

// 2.响应拦截
axios.interceptors.response.use(
  res => {
    //在响应的时候可以换掉token,以便更新令牌
    // ？？？
    // 更重要的是检查响应码
    return res
  },
  err => {
    if (err && err.response) {
      switch (err.response.status) {
        // case 400:err.message = '请求错误';break;
        case 401:router.push('/login');break;
          // case 403:err.message = '拒绝访问';break;
          // case 404:err.message = '请求页面不存在';break;
          // case 408:err.message = '请求超时';break;
          // case 500:err.message = '服务器错误';break;
          // case 504:err.message = '网络超时';break;
          // default:err.message = '前后端连接失败';
      }
    }
  }
)

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');