import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        tabBarIndex: 1 
    }
})
 
export default store

//思考：本地存储（sessionStorage、localStorage）与Vuex的区别 
// Vue:组件之间的传值
// 本地存储：用于页面之间的传值