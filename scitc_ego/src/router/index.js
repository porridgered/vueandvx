import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Scan from '../views/Scan.vue'
import Class from '../views/Class.vue'
import Cart from '../views/Cart.vue'
import News from '../views/News.vue'
import NewsShow from '../views/NewsShow.vue'
import My from '../views/My.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/class',
    name: 'Class',
    component: Class
  },
  {
    path: '/scan',
    name: 'Scan',
    component: Scan
  },
  {
    path: '/my',
    name: 'My',
    component: My
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/news',
    name: 'News',
    component: News
  },
  {
    path: '/newsShow/:id',
    name: 'NewsShow',
    component: NewsShow
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
