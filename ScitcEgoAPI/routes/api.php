<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    GoodsClassController,
    UserController,
    CarouselController,
    GoodsController,
    NewsController,
    ShopCarController
};
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
    //     return $request->user();
    // });
    // //没有经过认证就能访问：开放接口
    // // 认证：合法登录
    // Route::post('/login', [TeacherController::class,'login']);

    // // 如果数据需要保护，也就是需要登录后才能访问
    // Route::middleware('auth:api')->group(function () {
    //     Route::get('getTeacher',[TeacherController::class,'all']);
    //     Route::get('getuser',[TeacherController::class,'all']);

    // });

    // 认证机制：
    //web    session 会话 不能跨站 
    //Api    token 令牌 允许跨站

    
// （一）不需要认证就可以拿的东西
// 1、读取所有商品分类信息
Route::get('img',[GoodsController::class,'img']);
Route::get('getClass',[GoodsClassController::class,'all']);
Route::get('getClassWhithGoods',[GoodsClassController::class,'ClassWhithGoods']);

// 2、读取轮播图
Route::get('getCarousel',[CarouselController::class,'all']);

// 3、读取最新发布的商品
Route::get('getGoods',[GoodsController::class,'getGoods']);
Route::get('getGoodsAll',[GoodsController::class,'all']);

// 4、资讯
Route::get('getNews',[NewsController::class,'list']);
Route::get('news/{id}',[NewsController::class,'find']);

// 读取用户登录认证
Route::post('login',[Usercontroller::class,'login']);
// （二）需要认证（合法登录）才能访问
Route::middleware('auth:api')->group(function () {
    // 1、购物车
    Route::get('myShopCar',[ShopCarController::class,'all']);
    // 我的界面
    Route::get('my/{id}',[UserController::class,'getMyInfo']);
    Route::post('setFace',[UserController::class,'setFace']);   //上传头像

    // 2、我购买的物品
    // 3、我售卖的物品
    // 4、我发布的咨询
});

