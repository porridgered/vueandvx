<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// 商品分类模型
class GoodsClass extends Model
{
    use HasFactory;
    // protected $table = 'goods_classes';
    // protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $guarded = [];
    public $timestamps = false;
    
    // 建立后代
    function Goods()
    {
        return $this->hasMany(Goods::class,'cid','id');
    }
}
