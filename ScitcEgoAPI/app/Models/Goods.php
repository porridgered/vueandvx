<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    use HasFactory;
    // protected $table = 'goods';
    protected $guarded = [];
    public $timestamps = false;

    // 父类(属于)
    function GoodsClass(){
        return $this->belongsTo(GoodsClass::class,'cid','id');
    }
}
