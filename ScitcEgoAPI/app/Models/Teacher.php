<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Teacher extends Model
{
    use HasFactory;
    protected $table = 'teacher';
    protected $primaryKey = 'tid';
    protected $keyType = 'string';
    protected $guarded = [];
    public $timestamps = false;

    // 创建一个生成token的函数
    public function CreateToken($uid)
    {
        $token = Str::random(128);
        // 混合用户名，在进行哈希运算，生成一个不重复的令牌字符串
        $this->api_token = hash('sha256',$uid.$token);
        // 存入数据库：以便用户读取数据数据时携带令牌进行验证
        $this->save();
        // 派发，返给用户
        return $this->api_token;
    }

}
