<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $guarded = [];
    protected $keyType = 'string';
    public $timestamps = false;
    protected $hidden = ['password','api_token'];

    // 生成token
    public function CreateToken($id)
    {
        $tk = Str::random(128);
        $this->api_token = $id . hash('sha256',$tk);
        $this->save();
        return $this->api_token;
    }
}
