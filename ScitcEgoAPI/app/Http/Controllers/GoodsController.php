<?php

namespace App\Http\Controllers;
use App\Models\Goods;

use Illuminate\Http\Request;

class GoodsController extends Controller
{
    function getGoods(Request $req)
    {
       $count = $req->count ? $req->count : 10;
    //    $data = Goods::orderBy('add_time','desc')->paginate($count);
       $data = Goods::orderBy('add_time','desc')->take($count)->get();
       return response()->json([ 'code'=>1, 'data'=>$data]);
    }
    function all()
    {
        return Goods::all();
    }

}
