<?php

namespace App\Http\Controllers;
use App\Models\Carousel;        //模型制作

use Illuminate\Http\Request;

class CarouselController extends Controller     //控制器是做‘业务逻辑处理’
{
    function all()
    {
        return Carousel::all();
    }
}
