<?php

namespace App\Http\Controllers;
use App\Models\Teacher;
use GrahamCampbell\ResultType\Result;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    function login(Request $req)
    {
        $rule = [
            'uid' => 'required|string|max:20',
            'pwd' => 'required|string'
        ];
        $req->validate($rule);
        $teacher = Teacher::find($req->uid);
        if($teacher && Hash::check($req->pwd, $teacher->password)){
            // 如果账号密码都正确，则生成token，存储并派发给用户
            $token = $teacher->CreateToken($req->uid);
            return response()->json(['code'=>1,'token'=>$token]);
        }else{
            return response()->json(['code'=>0,'msg'=>'账号密码错误']);
        }
    }
    function all()
    {
        return Teacher::all();
    }
}
