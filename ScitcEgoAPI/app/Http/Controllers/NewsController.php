<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    function list()
    {
        $data = News::all();
        return response()->json(['code'=>1,'data'=>$data]);
    }

    function find($id)
    {
        $data = News::find($id);
        return response()->json(['code'=>1,'data'=>$data]);
    }
}
