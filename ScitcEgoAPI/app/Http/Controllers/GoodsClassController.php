<?php

namespace App\Http\Controllers;
use App\Models\GoodsClass;
use Illuminate\Http\Request;

// 商品分类控制器
class GoodsClassController extends Controller
{
    //读取所有商品分类数据
    function all()
    {
        return GoodsClass::orderBy('sort')->get();
    }
    // 读取商品分类及商品信息
    function ClassWhithGoods()
    {
        return GoodsClass::orderBy('sort')->with('Goods')->get();
    }
}
