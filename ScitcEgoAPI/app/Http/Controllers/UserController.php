<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;//存储文件使用


class UserController extends Controller
{
    function login(Request $req)
    {
        $req->validate([
            'id'=>'required|string|max:20',
            'pwd'=>'required|string',
        ]);
        
        $rs = ['code'=>0,'msg'=>'非法登录'];

        $user = User::find($req->id);
        if($user && Hash::check($req->pwd, $user->password)){
            $tk = $user->CreateToken($req->id);
            $rs = ['code'=>1,'token'=>$tk,'id'=>$user->id];
        }
        return response()->json($rs);
    }
    function getMyInfo($id)
    {
        return User::find($id);
    }
    function setFace(Request $req)      //更换头像
    {
        $face = $req->face;     //存储图片，更新数据库
        $type = $req->type;
        if($type == 'image/png'){
            $face = str_replace('data:image/png;base64,','',$face);     //需要对base64格式进行转换（去掉头）
        }else{
            $face = str_replace('data:image/jpeg;base64,','',$face);     //需要对base64格式进行转换（去掉头）
        }
        $face = str_replace('','+',$face);
        //把图片存储到指定位置
        $facepath = $req->id .Str::random(20) . '.jpg';
        // face目录需要在config/filesystems.php中配置
        Storage::disk('face')->put($facepath,base64_decode($face));  //base64_decode解码
        // 将新头像文件名更新到数据库
        $user = User::find($req->id);
        $oldFace = $user->face;
        $user->face = $facepath;
        $user->save();
        //删除旧文件
        Storage::disk('face')->delete($oldFace);
        return $facepath;
    }
}
