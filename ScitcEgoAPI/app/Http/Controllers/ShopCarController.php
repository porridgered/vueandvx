<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShopCar;

class ShopCarController extends Controller
{
    function all(Request $req)
    {
        $rs = ['code'=>0,'msg'=>'非法操作！'];
        if($req->id){
            $data = ShopCar::where('uid',$req->id)->orderBy('created_at','desc')
            ->with('goods')->get();//因为要显示商品信息，所以要携带查询商品数据
            $rs = ['code'=>1,'data'=>$data];
        }
        return response()->json($rs);
    }
}
