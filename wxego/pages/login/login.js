// 调用全局
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id : '',
        pwd : ''
    },
    // 实现登录
    login(){
        wx.request({
          url : app.Server + '/api/login',
          data : { id : this.data.id , pwd : this.data.pwd },
          method : 'post',
          success :  (res)=> {//js程序中，特别注意语句的写法，对this代表的含义的影响
              console.log('登录的结果',res);
            //   登录成功后记录token，下次访问携带
            wx.setStorageSync('token',res.data.token)//登录成功后记录token(存储到本地)，下次访问携带
            wx.setStorageSync('id', res.data.id)//用于取购物车等，属于自己的数据
            // 存储token后跳转到首页
            // wx.switchTab({})小程序navigateTo不允许跳转到tabBar页面
            // wx.reLaunch({url: 'url',})也可以使用
            wx.switchTab({
              url: '/pages/index/index',
            })
          },
          fail:function (err) {
              console.log('失败',err);
          }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})