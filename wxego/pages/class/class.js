// 要先引用api服务器地址，需要先引入app.js的内容
const app = getApp();
Page({
  
  /**   * 页面的初始数据   */
    data: {
        classList:[],//分类列表
        goodsList:[],//商品列表
        currList:[], // 筛选后的列表
        current:0// 代表当前选项
    },
  // 自定义方法,专门用于加载数据（页面第一次加载，和用户每次点击的时候调用）
    loadData(){
        // 要改变右边商品列表内容（思路：从原有数据里筛选）
        let newList = this.data.goodsList.filter((item)=>{
            return item.cid == this.data.current
        })
        this.setData({    currList: newList    })
    },

  // 自定义方法：setCurrent用于改变当前选中的分类
    setCurrent(ev){
        // 想得到的是索引号
        // console.log(ev.target.id)
        //改变data中的数据
        this.setData({ current:ev.target.id})
        this.loadData()
    },
    getClass(){     //查询所有分类
        wx.request({
            url : app.Server + '/api/getClass',        
            success: res =>{
                if(res.statusCode==200){
                    this.setData({ classList : res.data },()=>{
                        // 因为网络请求有迟延，所以应该在加载完所有数据之后再加载当前分类的商品信息
                        if (this.data.current==0 && res.data.length>0){
                            this.setData({ current : res.data[0].id })
                        }
                        this.loadData()
                    })
                }else{
                    console('数据读取失败');
                }
            },
            fail : err =>{
                console.log('查询失败！')
            }
        })

    },

    getGoods(){         //加载商品列表
        wx.request({
            url: app.Server + '/api/getGoodsAll',
            success : res =>{
                if (res.statusCode == 200){
                    let list = res.data
                    list.map((c)=>{
                        c.img = app.Server +  '/storage'+c.img
                    })
                    this.setData({ goodsList: list })
                    this.loadData()
                }else{
                    console.log('未读取到数据！');
                }
            },
            fail: err =>{
                console.log(err);
            }
        })
    },
    /*   * 生命周期函数--监听页面加载   */
    onLoad: function (options) {
        this.getClass()//先加载 分类列表数据
        this.getGoods()//加载商品信息
        if (options.cid){
            this.setData({ current: options.cid })
        }

    },

    /**   * 生命周期函数--监听页面初次渲染完成   */
    onReady: function () {

    },

    /**   * 生命周期函数--监听页面显示   */
    onShow: function () {

    },

    /**   * 生命周期函数--监听页面隐藏   */
    onHide: function () {

    },

    /**   * 生命周期函数--监听页面卸载   */
    onUnload: function () {

    },

    /**   * 页面相关事件处理函数--监听用户下拉动作   */
    onPullDownRefresh: function () {

    },

    /**   * 页面上拉触底事件的处理函数   */
    onReachBottom: function () {

    },

    /**   * 用户点击右上角分享   */
    onShareAppMessage: function () {

    }
})