// 调用全局
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsList: [],
    // 添加按钮的初始位置
    // 是否显示触底消息
    isShow: false
  },
  getNews() {
    wx.request({
      url: app.Server + '/api/getNews',
      success: res => {
        // 根据状态码判断当前请求有没有问题
        // console.log('状态码', res.data.data);
        let list = res.data.data
        list.map((c) => {
          c.img = app.Server + '/storage' + c.img
        })
        this.setData({
          newsList: list
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getNews()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // 导航条显示加载动画
    wx.showNavigationBarLoading()
    // 设置1.5秒隐藏加载动画，停止下拉
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 1500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // console.log('别拉了')
    // 当页面触底显示触底消息
    this.setData({
      isShow: true
    });
    // 此处还可以实现动态加载数据，每划一屏，请求加载一次数据
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})