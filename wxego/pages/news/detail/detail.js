// 调用全局
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: '', // 定义文章编号
        article: {} //文章本身
    },
    getdetail() {
        wx.request({
            url: app.Server + '/api/news/' + this.data.id,
            success: res => {
                // 根据状态码判断当前请求有没有问题
                // console.log('状态码', res.data.data);
                let list = res.data.data
                list.img =  app.Server + '/storage' + list.img
                this.setData({
                    article: list
                })
            },
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // 接收用户传递的文章编号
        this.setData({
            id: options.id
        })
        // 接收到用户要看的文章编号后，需要向后端请求查看信息
        // let news = {
        //     id: options.id,
        //     title:'测试小程序显示文章',
        //     date:'2021-6-2',
        //     hits:3234,
        //     img:'/img/touxiang/liuhaoran.jpg',
        //     content:' 世上人无完人，每个人有每个人的优点或缺点。而优雅对我们来说，他就是一种气质，是一种风度，是一种吸引。女人需要优雅，男人需要风度。轻言细语更好是风度翩翩良好结合。尊重别人，礼貌待人，自然也就有了优雅的格调。有时音乐也是人类的共同语言。没有音乐的世界是非常枯燥的，甚至会少了一些色彩。不喜欢音乐，不会欣赏音乐的人生或许是极其乏味的，甚至毫无意义。常听说：年轻就是一种心态。而童心，便是真诚与善良的结合。有这样的一份童心就会多一份快乐，少一些忧愁，所以你的人生便充满了色彩而使得更加年轻。有位哲人说过：我思故我在。其实沉思也是一种心灵的生活方式，也是心灵的清洗剂。它可以使人内心更加丰富而充实，可以使人更加淡定，可以使人成熟，可以使人更加的自信满满。'
        // }
        // 把从服务端取回的数据绑定到显示数据
        // this.setData({ article : news})
        this.getdetail()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})