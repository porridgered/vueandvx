// pages/shopcar/shopcar.js
const app = getApp();
Page({

    /** * 页面的初始数据 */
    data: {
        carList:[] ,   //购物车列表
        totalCount:0,
        totalPrice:0.00,
        isCKALL:false,
        slideButtons: [{
            type: 'warn',
            text: '删除',
            extClass: 'test',
            src: '/page/weui/cell/icon_del.svg',
        }],
    },
    slideButtonTap(e) {
        console.log('滑动删除', e)
    },
    // 计算总量
    setTotal(){
        // 选中商品为数据来源
        let list = this.data.carList.filter((car)=>{
            return car.state == true
        })
        // 遍历选中商品，计算总数量和总价格
        let count = 0;
        let price = 0;
        for(var i=0;i<list.length;i++){
            // count++        //计算种类
            count += list[i].count      // 计算总数
            price+=list[i].count * list[i].price
        }
        this.setData({totalCount:count,totalPrice:price})
    },
    // 点击改变状态
    setState(e){
        // console.log(e.target.id)
        // 找到当前商品，修改状态
        let list = this.data.carList
        list[e.target.id].state = !list[e.target.id].state    //等于自身状态相反，切换状态
        this.setData({ carList:list })  //把数据重新存回数据区
        // 调用计算总量
        this.setTotal()
    },
    // 点击改变数量
    setCount(e){
        let id = e.target.id
        let flg = e.target.dataset.flg
        // console.log(id,flg)
        let list = this.data.carList
        flg =='+'?list[id].count++:list[id].count--
        if(list[id].count <1){list[id].count=1}     //购买数量至少为1
        this.setData({ carList:list })
        // 计算总量
        this.setTotal()
    },
    // 全选
    ckAll(e){
        let ck = this.data.isCKALL;
        let list = this.data.carList;
        list.map((o)=>{
            o.state = !ck;
        })
        this.setData({ carList:list ,isCKALL:!ck})
        this.setTotal()
    },

    // 加载购物车数据
    loadDate(){
    if(wx.getStorageSync('token')){
        wx.request({
            url: app.Server +  '/api/myShopCar',
            data:{
                id : wx.getStorageSync('id') ,        //你是谁？
                api_token : wx.getStorageSync('token')      // 令牌？
            },
            success: res=>{
                if(res.statusCode == 200 && res.data.code ==1){
                    let list = res.data.data;
                    list.map(car=>{
                        car.goods.img =app.Server +  '/storage'+car.goods.img
                    })
                    this.setData({ carList : res.data.data })
                    console.log('购物车：',res)
                }else{
                    console.log('暂无数据');
                    console.log(wx.getStorageSync('id'))
                    console.log(wx.getStorageSync('token'))
                }
            },
            fail: err=>{
                console.log('数据传递错误！',err)
            }
        })
    }else{
        wx.navigateTo({
            url: '/pages/login/login',
        })
    }
    },

    /**   * 生命周期函数--监听页面加载   */
    onLoad: function (options) {
        let res = [
        // {carid:1,goodsImg:'/img/sj.png',goodsName:'华为手机',price:3000,count:2,state:true},
        // {carid:2,goodsImg:'/img/sjx.png',goodsName:'华为手机数据线',price:30,count:3,state:false},
        // {carid:3,goodsImg:'/img/sjk.png',goodsName:'华为手机手机壳',price:9.9,count:1,state:true},
        // {carid:4,goodsImg:'/img/pikaqiu.jpeg',goodsName:'皮卡丘电脑',price:3500,count:2,state:false},
        // {carid:5,goodsImg:'/img/cangshu.jpeg',goodsName:'仓鼠P30',price:2000,count:1,state:true},
        // {carid:6,goodsImg:'/img/sj.png',goodsName:'华为手机',price:3000,count:2,state:true},
        // {carid:7,goodsImg:'/img/sjx.png',goodsName:'华为手机数据线',price:30,count:2,state:false},
        // {carid:8,goodsImg:'/img/sjk.png',goodsName:'华为手机手机壳',price:9.9,count:3,state:true},
        // {carid:9,goodsImg:'/img/pikaqiu.jpeg',goodsName:'皮卡丘电脑',price:3500,count:2,state:false},
        // {carid:10,goodsImg:'/img/cangshu.jpeg',goodsName:'仓鼠P30',price:2000,count:1,state:true},
        ];
        // 选中状态默认为false
        // res.map((item)=>{
        //     return item.state = false
        // }),
        // this.setData({carList:res})
        this.setTotal()
    },

    /** * 生命周期函数--监听页面初次渲染完成 */
    onReady: function () {
        this.loadDate()

    },

    /**     * 生命周期函数--监听页面显示     */
    onShow: function () {

    },

    /**     * 生命周期函数--监听页面隐藏     */
    onHide: function () {

    },

    /**     * 生命周期函数--监听页面卸载     */
    onUnload: function () {

    },

    /**     * 页面相关事件处理函数--监听用户下拉动作     */
    onPullDownRefresh: function () {

    },

    /**     * 页面上拉触底事件的处理函数     */
    onReachBottom: function () {

    },

    /**     * 用户点击右上角分享     */
    onShareAppMessage: function () {

    }
})