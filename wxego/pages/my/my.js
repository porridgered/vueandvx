// pages/my/my.js
Page({
  /**   * 页面的初始数据   */
  data: {
    isOK:false,
    user:null
  },
  setOpen(){
    // 调用微信授权
    wx.getUserProfile({
      desc: '为了提供更好的服务，请先登录',
      success: (res)=>{
        console.log(res)
        this.setData({ user:res,isOK:true })
        // 获取用户授权后，可以存储用户信息，以便以后可以直接使用
        wx.setStorageSync('userinfo', res)
      }
    })
  },
  callTel(){
    wx.makePhoneCall({
      phoneNumber: '19160612324',
    })
  },
 
  /**   * 生命周期函数--监听页面加载   */
  onLoad: function (options) {
    // 读取用户授权数据
    let userinfo = wx.getStorageSync('userinfo')
    if(userinfo){
      this.setData({ user:userinfo,isOK:true })
    }
  
  },

  /**   * 生命周期函数--监听页面初次渲染完成   */
  onReady: function () {

  },

  /**   * 生命周期函数--监听页面显示   */
  onShow: function () {

  },

  /**   * 生命周期函数--监听页面隐藏   */
  onHide: function () {

  },

  /**   * 生命周期函数--监听页面卸载   */
  onUnload: function () {

  },

  /**   * 页面相关事件处理函数--监听用户下拉动作   */
  onPullDownRefresh: function () {

  },

  /**   * 页面上拉触底事件的处理函数   */
  onReachBottom: function () {

  },

  /**   * 用户点击右上角分享   */
  onShareAppMessage: function () {

  }
})