// 调用全局
const app = getApp();


Page({
    data:{
        cList : [],
        gList :[],  //商品列表（模拟数据）
        CarouselList: [],   //轮播图列表
        isLoad:false        //是否已经加载
    },
    getCarousel(){ // 加载轮播图数据
         wx.request({
            url : app.Server + '/api/getCarousel',
            success : (res)=>{
                let list = res.data
                list.map((c)=>{     //把图片地址更正为远程服务端地址
                    c.img = app.Server +  '/storage'+c.img
                })
                this.setData({ CarouselList: list })
                // console.log(this.data.CarouselList)
            },
            fail: (err)=>{
                console.log(err);
            }
        })
    },
    getClass(){     //加载产品分类
        wx.request({        //http请求
            url : app.Server + '/api/getClass',
            success : res =>{
                // 根据状态码判断当前请求有没有问题
                // console.log('状态码',res.statusCode);
                let list = res.data
                list.map((c)=>{
                    c.icon = app.Server +  '/storage'+c.icon
                })
                // console.log(list)
                this.setData({ cList: list })
            },
            fail: err =>{
                console.log(err);
            }
        })
    },
    getGoods(){         //加载商品列表
        wx.request({
            url: app.Server + '/api/getGoods',
            success : res =>{
                // 根据状态码判断当前请求有没有问题
                // console.log('状态码',res.statusCode);
                // console.log('取回数据',res.data)
                if (res.statusCode == 200 && res.data.code==1){
                    let list = res.data.data
                    list.map((c)=>{
                        c.img = app.Server +  '/storage'+c.img
                        c.name = c.name.substring(0,10)
                        c.intro = c.intro.substring(0,10)
                    })
                    // console.log(list)
                    this.setData({ gList: list })
                }else{
                    console.log('未读取到数据！');
                }
            },
            fail: err =>{
                console.log(err);
            }
        })
    },
    // 声明周期函数和：页面加载时执行,只加载一次
    onLoad: function(){
        // 不需要验证
       this.getCarousel();
       this.getClass();
       this.getGoods();

    },

    // 页面显示的时候（每次切换回来都要加载）
    // onShow () {
    //     if(wx.getStorageSync('token')){     // 先检查检测是否已经认证，如果没有，则跳转到登录认证页面
    //         // console.log(wx.getStorageSync('token'));
    //         // console.log(wx.getStorageSync('id'));
    //        this.loadDate()
    //        this.setData({ isLoad:true })
    //     }else{      //如果没有认证则跳转到登录页面去登录验证
    //         wx.navigateTo({
    //           url: '/pages/login/login',
    //         })
    //     }
    // },
    // loadDate(){
    //     if(this.data.isLoad) return false;
    //     // console.log(111)
    //      wx.request({ // 调用laravel API读取数据库中的数据
    //         url: app.Server +  '/api/getClass',
    //         //要获得服务器数据，访问时携带令牌token
    //         data: {
    //             api_token:wx.getStorageSync('token'),
    //             id:wx.getStorageSync('id')
    //         },
    //         success:(res)=>{
    //         console.log('服务端返回的数据',res.data)
    //         // 取回数据做个修改：图片地址
    //         let list = res.data
    //         list.map((c)=>{
    //             c.icon = app.Server +  '/storage'+c.icon
    //         })
    //         this.setData({cList : list})
    //         },
    //         fail:(err)=> {
    //             console.log('远程调用失败',err)
    //         }
    //     })
        
    // },

    
})