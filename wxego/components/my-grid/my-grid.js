// components/my-grid/my-grid.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
      // 为了产生多个菜单
      itemList:{
        type:Array
      }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
